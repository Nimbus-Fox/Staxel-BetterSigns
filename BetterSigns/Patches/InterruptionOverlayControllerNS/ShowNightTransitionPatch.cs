﻿namespace BetterSigns.Patches.InterruptionOverlayControllerNS
{
	internal class ShowNightTransitionPatch
	{
		/// <summary>
		/// Make sure that whenever the player faints we close the UI if its open
		internal static void PreShowNightTransition()
		{
			if(BetterSigns.Instance.SignController.IsVisible())
			{
				BetterSigns.Instance.SignController.Hide();
			}
		}
	}
}
