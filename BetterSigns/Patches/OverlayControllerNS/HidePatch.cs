﻿namespace BetterSigns.Patches.OverlayControllerNS
{
	internal class HidePatch
	{
		/// <summary>
		/// Make sure that we hide our UI if it is globally called on OverlayController
		/// </summary>
		internal static void PreHide()
		{
			BetterSigns.Instance.SignController.Hide();
		}
	}
}
