﻿namespace BetterSigns.Patches.OverlayControllerNS
{
	internal class OnHideAllPatch
	{
		/// <summary>
		/// Make sure that we cancel the method from connector.js Connections.pressBack since we 
		/// do not register our js module within it
		/// </summary>
		internal static bool PreOnHideAll()
		{
			if(BetterSigns.Instance.SignController.IsVisible())
			{
				return false;
			}

			return true;
		}
	}
}
