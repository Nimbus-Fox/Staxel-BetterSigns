﻿using Staxel;

namespace BetterSigns.Patches.OverlayControllerNS
{
	internal class UpdateModalModesPatch
	{
		/// <summary>
		/// Make sure that we AcquireInputControl whenever our menu opens
		/// </summary>
		internal static void AfterUpdateModalModes()
		{
			if(!ClientContext.WebOverlayRenderer.HasInputControl())
			{
				bool capture = BetterSigns.Instance.SignController.CaptureInput();
				if(capture)
				{
					ClientContext.WebOverlayRenderer.AcquireInputControl();
				}
			}
		}
	}
}
