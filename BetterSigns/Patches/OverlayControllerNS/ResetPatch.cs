﻿namespace BetterSigns.Patches.OverlayControllerNS
{
	internal class ResetPatch
	{
		/// <summary>
		/// Make sure that we hide our UI if it is globally called on OverlayController
		/// </summary>
		internal static void PreReset()
		{
			BetterSigns.Instance.SignController.Hide();
			BetterSigns.Instance.SignController.Reset();
		}
	}
}
