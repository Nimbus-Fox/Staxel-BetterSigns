﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection.Emit;
using System.Threading;
using BetterSigns.Classes;
using BetterSigns.Patches.InterruptionOverlayControllerNS;
using BetterSigns.Patches.OverlayControllerNS;
using CefSharp;
using Harmony;
using Microsoft.Xna.Framework.Input;
using NimbusFox.FoxCore;
using NimbusFox.FoxCore.Classes;
using Plukit.Base;
using Staxel;
using Staxel.Browser;
using Staxel.Client;
using Staxel.Core;
using Staxel.Draw;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Modding;
using Staxel.Tiles;

namespace BetterSigns {
    public class BetterSigns : IModHookV3 {
        public static BetterSigns Instance { get; private set; }

        public UIController SignController;

        internal static Fox_Core FxCore;

        static BetterSigns() {
            FxCore = new Fox_Core("Barrybadpak", "BetterSigns FoxCore", "v0.1");

            FxCore.PatchController.Override(typeof(BrowserRenderSurface), "Init", typeof(BetterSigns), nameof(AfterInit), AfterInitTranspiler);

            FxCore.PatchController.Add(typeof(InterruptionOverlayController), "ShowNightTransition",
                typeof(ShowNightTransitionPatch), nameof(ShowNightTransitionPatch.PreShowNightTransition));

            FxCore.PatchController.Add(typeof(OverlayController), "Hide", typeof(HidePatch), nameof(HidePatch.PreHide));
            FxCore.PatchController.Add(typeof(OverlayController), "OnHideAll", typeof(OnHideAllPatch), nameof(OnHideAllPatch.PreOnHideAll));
            FxCore.PatchController.Add(typeof(OverlayController), "Reset", typeof(ResetPatch), nameof(ResetPatch.PreReset));
            FxCore.PatchController.Add(typeof(OverlayController), "UpdateModalModes",
                typeof(UpdateModalModesPatch), nameof(UpdateModalModesPatch.AfterUpdateModalModes));
        }

        private string HtmlAsset { get; set; }
        private string JsAsset { get; set; }
        private string CssAsset { get; set; }

        private static BrowserRenderSurface StartMenuSurface { get; set; }
        private static BrowserRenderSurface OverlaySurface { get; set; }
        private static readonly List<UIReferencePair> _uiPairs = new List<UIReferencePair>();

        public BetterSigns() {
            Instance = this;

            var uiDir = FxCore.ModDirectory.FetchDirectory("UI");

            HtmlAsset = uiDir.ReadFile<string>("index.min.html", true).Replace('"', '\'');
            JsAsset = uiDir.ReadFile<string>("main.min.js", true);
            CssAsset = uiDir.ReadFile<string>("style.min.css", true);
        }

        public void ClientContextInitializeInit() { }

        /// <summary>
		/// We can only instantiate the controller after the ClientContext is initialised
		/// otherwise the WeboverlayRenderer is not available
		/// </summary>
		public void ClientContextInitializeBefore() {
            this.SignController = new UIController();
        }

        public void ClientContextInitializeAfter() { }
        public void ClientContextDeinitialize() { }
        public void ClientContextReloadBefore() { }
        public void ClientContextReloadAfter() { }
        public void CleanupOldSession() { }

        internal static void FrameLoadEnd(object sender, FrameLoadEndEventArgs e) {
            for (var i = _uiPairs.Count - 1; i >= 0; i--) {
                var uiPair = _uiPairs[i];

                if (uiPair.Browser.Address.Contains("startmenu.html")) {
                    StartMenuSurface = uiPair.Surface;
                    _uiPairs.RemoveAt(i);
                    continue;
                }

                if (uiPair.Browser.Address.Contains("overlay.html")) {
                    OverlaySurface = uiPair.Surface;
                    _uiPairs.RemoveAt(i);
                    continue;
                }
            }


            if (e.Url.Contains("startmenu.html") && StartMenuSurface != null) {
                Instance.StartMenuUiLoaded(StartMenuSurface);
                return;
            }

            if (e.Url.Contains("overlay.html") && OverlaySurface != null) {
                Instance.IngameOverlayUiLoaded(OverlaySurface);
                return;
            }
        }

        internal static IEnumerable<CodeInstruction> AfterInitTranspiler(IEnumerable<CodeInstruction> instructions) {
            int targetIdx = -1;
            List<CodeInstruction> codes = new List<CodeInstruction>(instructions);

            for (int i = 0; i < codes.Count; i++) {

                if (codes[i].opcode == OpCodes.Stfld && codes[i].operand.ToString() == "Boolean _initialized"
                                                     && codes[i - 1].opcode == OpCodes.Ldc_I4_1) {
                    targetIdx = i;
                    continue;
                }
            }

            if (targetIdx != -1) {
                codes[targetIdx - 2].opcode = OpCodes.Nop; // Otherwise we're removing a jump target
                codes.RemoveRange(targetIdx - 1, 2);
            }

            return codes.AsEnumerable();
        }

        internal class AssetSchemeHandler : IResourceHandler, IDisposable {
            public static string AssetPrefix = "staxel/ui";
            private MemoryStream _stream;
            private string _mimeType;

            bool IResourceHandler.ProcessRequest(IRequest request, ICallback callback) {
                try {
                    Uri uri = new Uri(request.Url);
                    if (uri.Host != "gameassets") {
                        if (uri.IsFile)
                            callback.Cancel();
                        return true;
                    }
                    string str = Uri.UnescapeDataString(uri.AbsolutePath);
                    string resource = str.StartsWith("/mods/") || str.StartsWith("/staxel/") ? str.Substring(1) : AssetSchemeHandler.AssetPrefix + str;
                    if (resource.EndsWith("/"))
                        resource += "index.html";
                    if (this._stream != null)
                        Allocator.MemoryStreamAllocator.Release(ref this._stream);
                    ContentLoader contentLoader = GameContext.ContentLoader;
                    if (contentLoader == null)
                        throw new Exception("Failed to request asset for the browser, contentloader not available. (path:" + resource + ")");
                    if (GameContext.AssetBundleManager == null)
                        throw new Exception("Failed to request asset for the browser, GameContext.AssetBundleManager not available. (path:" + resource + ")");
                    this._stream = contentLoader.ReadStream(resource);
                    string extension = resource;
                    int startIndex = extension.LastIndexOf('.');
                    if (startIndex != -1)
                        extension = extension.Substring(startIndex);
                    this._mimeType = ResourceHandler.GetMimeType(extension);
                    callback.Continue();
                    return true;
                } catch (Exception ex) {
                    Logger.LogException("Failed to load request:" + request.Url, ex);
                    callback.Cancel();
                }
                return true;
            }

            void IResourceHandler.GetResponseHeaders(IResponse response, out long responseLength, out string redirectUrl) {
                redirectUrl = (string)null;
                response.StatusCode = 200;
                response.StatusText = "OK";
                response.MimeType = this._mimeType;
                responseLength = (long)(int)this._stream.Length;
            }

            bool IResourceHandler.ReadResponse(Stream dataOut, out int bytesRead, ICallback callback) {
                callback.Dispose();
                int length = (int)Math.Min(dataOut.Length, this._stream.Remainder());
                StreamExtensions.MemoryStreamCopyTo(this._stream, dataOut, length);
                bytesRead = length;
                if (length != 0)
                    return true;
                Allocator.MemoryStreamAllocator.Release(ref this._stream);
                return false;
            }

            bool IResourceHandler.CanGetCookie(Cookie cookie) {
                return true;
            }

            bool IResourceHandler.CanSetCookie(Cookie cookie) {
                return true;
            }

            void IResourceHandler.Cancel() {
            }

            public void Dispose() {
                if (this._stream == null)
                    return;
                Allocator.MemoryStreamAllocator.Release(ref this._stream);
            }
        }

        internal class CefSharpSchemeHandlerFactory : ISchemeHandlerFactory {
            public const string SchemeName = "asset";

            public IResourceHandler Create(IBrowser browser, IFrame frame, string schemeName, IRequest request) {
                return (IResourceHandler)new AssetSchemeHandler();
            }
        }

        internal static void AfterInit(BrowserRenderSurface __instance, DeviceContext context, Vector2I? viewPortSize = null) {
            var initialized = __instance.GetPrivateFieldValue<bool>("_initialized");
            if (BrowserCore.Disabled)
                return;
            if (viewPortSize.HasValue && (viewPortSize.Value.X > 4096 || viewPortSize.Value.Y > 4096))
                throw new Exception("Viewport must not exceed 4096x4096 due to XNA limitations.");
            if (viewPortSize.HasValue && (viewPortSize.Value.X <= 0 || viewPortSize.Value.Y <= 0))
                throw new Exception("Viewport must not be smaller than 1x1 : " + (object)viewPortSize.Value.X + "," + (object)viewPortSize.Value.Y);
            if (initialized)
                return;

            __instance.SetPrivateFieldValue("_initialized", true);
            __instance.SetPrivateFieldValue("_processMessagesAction", new Action<int, IntPtr, IntPtr, MouseState>(__instance.ProcessMessages));
            ClientContext.InputSource.RegisterMessageHandler(__instance.GetPrivateFieldValue<Action<int, IntPtr, IntPtr, MouseState>>("_processMessagesAction"));
            __instance.SetPrivateFieldValue("_scrollMouseAction", new Action<int, MouseState>((i, state) => {
                AccessTools.Method(__instance.GetType(), "ScrollMouse").Invoke(__instance, new object[] {i, state});
            }));
            ClientContext.InputSource.RegisterScrollHandler(__instance.GetPrivateFieldValue<Action<int, MouseState>>("_scrollMouseAction"));
            var browserSettings = new BrowserSettings {
                WindowlessFrameRate = 60,
                CaretBrowsing = CefState.Enabled
            };
            __instance.SetPrivateFieldValue("_requestContext", new RequestContext(new RequestContextSettings() {
                CachePath = "cache",
                PersistSessionCookies = false
            }));

            var requestContext = __instance.GetPrivateFieldValue<RequestContext>("_requestContext");

            requestContext.RegisterSchemeHandlerFactory("asset", "gameassets", (ISchemeHandlerFactory)new CefSharpSchemeHandlerFactory());
            ClientContext.BrowserCore.CheckInitialized();
            __instance.SetPrivateFieldValue("_browser", new ChromiumWebBrowser("asset://gameassets/empty.html", browserSettings, requestContext));

            var _browser = __instance.GetPrivateFieldValue<ChromiumWebBrowser>("_browser");
            _browser.FrameLoadEnd += (e, args) => {
                AccessTools.Method(__instance.GetType(), "FrameLoadEnd").Invoke(__instance, new object[] {e, args});
            };
            _browser.Initialize();
            while (!_browser.IsBrowserInitialized) {
                ClientContext.BrowserCore.Update();
                Thread.Sleep(1);
            }
            __instance.SetPrivateFieldValue("_fixedViewPortSize", viewPortSize);

            AccessTools.Method(__instance.GetType(), "Resize").Invoke(__instance, new object[] {context});

            var pair = new UIReferencePair(_browser, __instance);
            if (!_uiPairs.Contains(pair)) {
                _uiPairs.Add(pair);
                _browser.FrameLoadEnd += FrameLoadEnd;
            }
        }

        /// <summary>
		/// Inject the UI contents
		/// </summary>
		internal void IngameOverlayUiLoaded(BrowserRenderSurface surface) {
            surface.CallPreparedFunction("(() => { const el = document.createElement('style'); el.type = 'text/css'; el.appendChild(document.createTextNode('" + this.CssAsset + "')); document.head.appendChild(el); })();");
            surface.CallPreparedFunction("$('body').append(\"" + this.HtmlAsset + "\");");
            surface.CallPreparedFunction("try{ " + this.JsAsset + " } catch(e) { alert(e.toString()); }");

            this.SignController.Reset();
        }

        internal void StartMenuUiLoaded(BrowserRenderSurface surface) {

        }

        /// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
        public void Dispose() { }
        public void GameContextInitializeInit() { }
        public void GameContextInitializeBefore() { }
        public void GameContextInitializeAfter() { }
        public void GameContextDeinitialize() { }
        public void GameContextReloadBefore() { }
        public void GameContextReloadAfter() { }
        public void UniverseUpdateBefore(Universe universe, Timestep step) { }
        public void UniverseUpdateAfter() { }
        public bool CanPlaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
            return true;
        }

        public bool CanReplaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
            return true;
        }

        public bool CanRemoveTile(Entity entity, Vector3I location, TileAccessFlags accessFlags) {
            return true;
        }

        public bool CanInteractWithTile(Entity entity, Vector3F location, Tile tile) {
            return true;
        }

        public bool CanInteractWithEntity(Entity entity, Entity lookingAtEntity) {
            return true;
        }
    }
}
